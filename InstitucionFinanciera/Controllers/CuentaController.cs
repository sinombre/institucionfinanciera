﻿using InstitucionFinanciera.Models;
using InstitucionFinanciera.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InstitucionFinanciera.Controllers
{
    [EnableCors("politicaCORS")]
    [Route("api/Customer/{idCustomer}/[controller]")]    
    [ApiController]
    public class CuentaController : ControllerBase
    {
        IRepository _repository;

        public CuentaController(IRepository repository)
        {
            _repository = repository;
        }
                        
        [HttpGet]
        public ActionResult<Anuncio<IEnumerable<Cuenta>>> Get(int idCustomer)
        {
            return _repository.GetCuentas(idCustomer);
        }

        [EnableCors("politicaCORS")]
        [HttpPost]
        public ActionResult<Anuncio<Cuenta>> Post([FromBody] Cuenta cuenta, int idCustomer)
        {
            Anuncio<Cuenta> anuncio = new Anuncio<Cuenta>();
            cuenta.CustomerId = idCustomer;

            if (ModelState.IsValid)
            {
                return _repository.AltaCuenta(cuenta);
            }

            return BadRequest(anuncio);
        }
        
    }
}
