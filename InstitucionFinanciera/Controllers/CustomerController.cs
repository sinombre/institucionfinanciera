﻿using InstitucionFinanciera.Models;
using InstitucionFinanciera.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InstitucionFinanciera.Controllers
{
    [EnableCors("politicaCORS")]
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        IRepository _repository;

        public CustomerController(IRepository repository)
        {
            _repository = repository;
        }
                
        [EnableCors("politicaCORS")]
        [HttpGet]
        public ActionResult< Anuncio<IEnumerable<Customer>>> Get()
        {
            return _repository.GetCustomer();
        }
                
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }
                        
        [HttpPost]
        public ActionResult<Anuncio<Customer>> Post([FromBody] Customer customer)
        {
            Anuncio<Customer> anuncio = new Anuncio<Customer>();
            
            if (ModelState.IsValid)
            {
                return _repository.AltaCustomer(customer);                
            }

            return BadRequest(anuncio);            
        }        
    }
}
