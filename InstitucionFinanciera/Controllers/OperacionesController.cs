﻿using InstitucionFinanciera.Models;
using InstitucionFinanciera.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InstitucionFinanciera.Controllers
{
    [EnableCors("politicaCORS")]
    [Route("api/Cuenta/{idCuenta}/[controller]")]
    [ApiController]
    public class OperacionesController : ControllerBase
    {
        IRepository _repository;

        public OperacionesController(IRepository repository)
        {
            _repository = repository;
        }        
        
        [HttpGet]
        public ActionResult<Anuncio<IEnumerable<Operacion>>> Get(int idCuenta)
        {
            return _repository.GetOperaciones(idCuenta);
        }
        
        [HttpPost]
        public ActionResult<Anuncio<Cuenta>> Post([FromBody] Operacion operacion, int idCuenta)
        {
            Anuncio<Cuenta> anuncio = new Anuncio<Cuenta>();
            operacion.Fecha = DateTime.Now;
            operacion.IdCuenta = idCuenta;

            if (ModelState.IsValid)
            {                
                 return _repository.GuardarTransaccion(operacion);
            }

            return BadRequest(anuncio);
        }
    }
}
