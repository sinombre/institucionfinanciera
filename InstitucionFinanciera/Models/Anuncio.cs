﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InstitucionFinanciera.Models
{
    public class Anuncio<T>
    {
        public ClasificacionAnuncio Clasificacion { get; set; }

        public string Error { get; set; }

        public T Detalle { get; set; }

        public Anuncio()
        {
            Clasificacion = ClasificacionAnuncio.Success;
        }

    }

    public enum ClasificacionAnuncio
    {
        Error = 0,
        Success = 1,        
        Notificacion = 2        
    }

    public enum ClasificacionOperacion
    {        
        Deposito = 1,
        Retiro = 2
    }
}
