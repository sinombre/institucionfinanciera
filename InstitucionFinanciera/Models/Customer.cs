﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace InstitucionFinanciera.Models
{
    public class Customer
    {
        public int Id { get; set; }

        [StringLength(57)]
        public string Nombre { get; set; }

        public int Folio { get; set; }

        [JsonIgnore]
        public virtual IEnumerable<Cuenta> Cuentas { get; set; }
    }
}
