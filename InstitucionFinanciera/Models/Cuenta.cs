﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace InstitucionFinanciera.Models
{
    public class Cuenta
    {
        public int Id { get; set; }

        public decimal Saldo { get; set; }

        [ForeignKey("Customer")]
        public int CustomerId { get; set; }

        [JsonIgnore]
        public virtual Customer Customer { get; set; }

        [JsonIgnore]
        public virtual IEnumerable<Operacion> Operaciones { get; set; }

    }
}
