﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace InstitucionFinanciera.Models
{
    public class Operacion
    {
        public int Id { get; set; }

        [Range(1, 2)]
        public int Clasificacion { get; set; }

        public decimal Monto { get; set; }

        [ForeignKey("Cuenta")]
        public int IdCuenta { get; set; }

        public DateTime Fecha { get; set; }

        [NotMapped]
        public string FechaLetra
        {
            get
            {
                return Fecha != null ? Fecha.ToString() : "";
            }
        }

        public virtual Cuenta Cuenta { get; set; }

    }
}
