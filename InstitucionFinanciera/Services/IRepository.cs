﻿using InstitucionFinanciera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InstitucionFinanciera.Services
{
    public interface IRepository
    {
        Anuncio<Cuenta> AltaCuenta(Cuenta cuenta);

        Anuncio<Cuenta> GuardarTransaccion(Operacion operacion);        

        Anuncio<Customer> AltaCustomer(Customer customer);

        Anuncio<IEnumerable<Customer>> GetCustomer();        

        Anuncio<IEnumerable<Cuenta>> GetCuentas(int idCliente);
                
        Anuncio<IEnumerable<Operacion>> GetOperaciones(int cuenta);

    }
}
