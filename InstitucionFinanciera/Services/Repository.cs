﻿using InstitucionFinanciera.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InstitucionFinanciera.Services
{
    public class Repository : IRepository
    {
        private AplicationDbContext db;        

        public Repository(AplicationDbContext context)
        {
            db = context;
        }

        public Anuncio<Cuenta> AltaCuenta(Cuenta cuenta)
        {
            Anuncio<Cuenta> anuncio = new Anuncio<Cuenta>();
            try
            {
                db.Cuentas.Add(cuenta);
                db.SaveChanges();
                anuncio.Detalle = cuenta;
                return anuncio;
            }
            catch (Exception error)
            {
                anuncio.Clasificacion = ClasificacionAnuncio.Error;
                anuncio.Error = error.Message;
                return anuncio;
            }            
        }

        public Anuncio<Customer> AltaCustomer(Customer customer)
        {
            Anuncio<Customer> anuncio = new Anuncio<Customer>();

            try
            {               
                db.Customers.Add(customer);
                db.SaveChanges();
                anuncio.Detalle = customer;
                return anuncio;
            }
            catch (Exception error)
            {
                anuncio.Clasificacion = ClasificacionAnuncio.Error;
                anuncio.Error = error.Message;
                return anuncio;
            }
        }

        public Anuncio<IEnumerable<Cuenta>> GetCuentas(int idCliente)
        {
            Anuncio<IEnumerable<Cuenta>> anuncio = new Anuncio<IEnumerable<Cuenta>>();
            
            try
            {
                anuncio.Detalle = db.Cuentas.Where(x => x.CustomerId == idCliente).ToList();
                return anuncio;
            }
            catch (Exception error)
            {
                anuncio.Clasificacion = ClasificacionAnuncio.Error;
                anuncio.Error = error.Message;
                return anuncio;
            }            
        }

        public Anuncio<IEnumerable<Customer>> GetCustomer()
        {
            Anuncio<IEnumerable<Customer>> anuncio = new Anuncio<IEnumerable<Customer>>();

            try
            {
                anuncio.Detalle = db.Customers.ToList();
                return anuncio;
            }
            catch (Exception error)
            {
                anuncio.Clasificacion = ClasificacionAnuncio.Error;
                anuncio.Error = error.Message;
                return anuncio;
            }
        }

        public Anuncio<Cuenta> GuardarTransaccion(Operacion operacion)
        {
            Anuncio<Cuenta> anuncio = new Anuncio<Cuenta>();

            try
            {
                Cuenta cuenta = db.Cuentas.FirstOrDefault(x => x.Id == operacion.IdCuenta);
                operacion.Fecha = DateTime.Now;

                if (cuenta != null)
                {
                    if (operacion.Clasificacion == (int)ClasificacionOperacion.Retiro)
                    {
                        if (operacion.Monto > cuenta.Saldo)
                        {
                            anuncio.Clasificacion = ClasificacionAnuncio.Error;
                            anuncio.Error = "Saldo insuficiente puedes retirar hasta " + cuenta.Saldo;
                            return anuncio;
                        }
                        else
                        {
                            cuenta.Saldo = cuenta.Saldo - operacion.Monto;
                        }
                    }

                    if (operacion.Clasificacion == (int)ClasificacionOperacion.Deposito)
                    {
                        cuenta.Saldo = cuenta.Saldo + operacion.Monto;
                    }

                    db.Operacions.Add(operacion);
                    db.Cuentas.Attach(cuenta);
                    db.Entry(cuenta).State = EntityState.Modified;
                    db.SaveChanges();

                    anuncio.Detalle = cuenta;
                    return anuncio;
                }

                anuncio.Clasificacion = ClasificacionAnuncio.Error;
                anuncio.Error = "no se encontro una cuenta asociada para aplicar la transaccion";
                return anuncio;
            }
            catch (Exception error)
            {
                anuncio.Clasificacion = ClasificacionAnuncio.Error;
                anuncio.Error = error.Message;
                return anuncio;
            }
        }

        public Anuncio<IEnumerable<Operacion>> GetOperaciones(int cuenta)
        {
            Anuncio<IEnumerable<Operacion>> anuncio = new Anuncio<IEnumerable<Operacion>>();

            try
            {
                anuncio.Detalle = db.Operacions.Where(x => x.IdCuenta == cuenta).OrderByDescending(x => x.Fecha).ToList();
                return anuncio;
            }
            catch (Exception error)
            {
                anuncio.Clasificacion = ClasificacionAnuncio.Error;
                anuncio.Error = error.Message;
                return anuncio;
            }
        }
    }
}
