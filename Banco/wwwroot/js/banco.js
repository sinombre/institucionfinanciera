﻿var ubicacion = 'https://localhost:44317/api/'
var clasificacionAnuncio = { success:1 }

function GetCustomers()
{
    $.ajax({
        type: 'GET',
        url: ubicacion + 'Customer',        
        async: true
    }).done(function (data) {
        
        ManejadorUniversal(data, PintaCustomers)

    }).fail(function (error) {
        Swal.fire('error', error, 'error');
    }); 
}

var PintaCustomers = function (clientes)
{    
    var tableContent = $('#clientes');
    tableContent.empty()

    for (var i = 0; i < clientes.length; i++) {
        var row = $('<tr>' +
            '<td>' + clientes[i].folio + '</th>' +
            '<td>' + clientes[i].nombre + '</td>' +
            '<td><button type="button" step="any" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" onClick="PintaCuentas(' + clientes[i].id + ')">Ver cuentas</button</td>' +
            '</tr>');
        tableContent.append(row);
    }

};

var PintaCuentas = function (idCliente, nombre)
{    
    var detalle = $('#detalle')

    detalle.empty();
    detalle.append('<p>Añadir cuenta de ahorro</p>')

    var cuentaRegistro = $('<form>' +
        '<div class= "form-group" >                                                                                   ' +
        '<label for="Saldo">saldo</label>                                                        ' +
        '<input type="number" class="form-control" id="Saldo" name="Saldo" aria-describedby="SaldoHelp">               ' +
        '    <small id="SaldoHelp" class="form-text text-muted"></small>' +
        '</div>' +        
        '</form>');

    var cuentaBoton = $('<button type="button" class="btn btn-primary">Guardar</button>');
        
    cuentaBoton.click(function () {
        
        var saldo = $('#Saldo').val()

        if (saldo && saldo >= 0) {

            var cuenta = new CuentaFactory();
            cuenta.Saldo = parseFloat(saldo)

            $.ajax({
                type: "POST",
                contentType: 'application/json',
                dataType: 'json',
                url: ubicacion + 'Customer/' + idCliente + '/Cuenta',
                data: JSON.stringify(cuenta),
                timeout: 100000,
                success: function (data)
                {                    
                    ManejadorUniversal(data, function (x)
                    {
                        Swal.fire('¡Correcto!', 'cuenta guardada', 'success')
                        PintaCuenta(x)
                    })
                },
                error: function (e) {
                    Swal.fire('error', error, 'error');
                },
                done: function (e) {
                }
            });
        }
        else {
            Swal.fire('error',        'especifica un saldo',        'error');
        }
    });

    cuentaRegistro.append(cuentaBoton);
    detalle.append(cuentaRegistro);

    GetCuentas(idCliente);

}

var GetCuentas = function (idCliente, urlCuenta)
{
    $.ajax({
        type: 'GET',
        url: ubicacion +'Customer/'+ idCliente+'/Cuenta',
        async: true
    }).done(function (data) {        

        ManejadorUniversal(data, function (x)
        {
            $('#exampleModal .modal-dialog').css('max-width', '700px')
            CuentasRender(x)
        })

    }).fail(function (error) {
        Swal.fire('error', error, 'error');
    });
};

var CuentasRender = function (cuentas) {
    var tableCuenta = $('<table class="table">' +
        '<thead>' +
        '<tr>                                ' +
        '    <th scope="col"># Cuenta</th>          ' +
        '    <th scope="col">Saldo</th>      ' +
        '    <th scope="col">Deposito</th>       ' +
        '    <th scope="col">Retiro</th>     ' +
        '    <th scope="col">Historial</th>     ' +
        '</tr>                               ' +
        '</thead></table>                             ');

    var tableContent = $('<tbody id="cuentas" ></tbody>');

    for (var i = 0; i < cuentas.length; i++) {
        var rowCuenta = $('<tr>' +
            '<td class="cuenta" >' + cuentas[i].id + '</th>' +
            '<td class="saldo">' + cuentas[i].saldo + '</td>' +
            '</tr>');

        var depositoCelda = $('<td></td>');
        var divDeposito = $('<div class="row envoltorio"></div>');
        var depositoInput = $('<div class="col-sm-12"><input type="number" step="any" class="form-control cantidad"></div>');
        var depositoEnvoltorio = $('<div class="col"></div>');
        var depositoBoton = $('<button type="button" class="btn btn-success"> Depositar</button>');
        depositoBoton.click(Depositar(cuentas[i], 1));
        depositoEnvoltorio.append(depositoBoton);
        divDeposito.append(depositoInput);
        divDeposito.append(depositoEnvoltorio);
        depositoCelda.append(divDeposito);
        rowCuenta.append(depositoCelda);

        var retiroCelda = $('<td></td>');
        var divRetiro = $('<div class="row envoltorio"></div>');
        var retiroInput = $('<div class="col-sm-12"><input type="number" step="any" class="form-control cantidad"></div>');
        var retiroEnvoltorio = $('<div class="col"></div>');
        var retiroBoton = $('<button type="button" class="btn btn-danger  ">Retirar</button>');
        retiroBoton.click(Depositar(cuentas[i], 2));
        retiroEnvoltorio.append(retiroBoton);
        divRetiro.append(retiroInput);
        divRetiro.append(retiroEnvoltorio);
        retiroCelda.append(divRetiro);
        rowCuenta.append(retiroCelda);        
                
        var movimientos = $('<td><button type="button" data-toggle="modal" data-target="#detalleOperaciones" onClick="PintaMovimientos(' + cuentas[i].id + ')" class="btn btn-primary">Movimientos</button></td>')
        rowCuenta.append(movimientos);              

        tableContent.append(rowCuenta);
    }

    tableCuenta.append(tableContent);

    $('#detalle').append(tableCuenta);
};

var CuentaFactory = function () {
    return { Id: 0,  Saldo: 0, CustomerId: 0 };
};

var PintaCuenta = function (cuenta) {
    var cuentas = $('#cuentas')

    var rowCuenta = $('<tr>' +
        '<td class="cuenta">' + cuenta.id + '</th>' +
        '<td class="saldo">' + cuenta.saldo + '</td>' +
        '</tr>');

    var depositoCelda = $('<td></td>');
    var divDeposito = $('<div class="row envoltorio"></div>');
    var depositoInput = $('<div class="col-sm-6"><input type="number" step="any" class="form-control cantidad"></div>');
    var depositoEnvoltorio = $('<div class="col"></div>');
    var depositoBoton = $('<button type="button" class="btn btn-success"> Depositar</button>');
    depositoBoton.click(Depositar(cuenta, 1));
    depositoEnvoltorio.append(depositoBoton);
    divDeposito.append(depositoInput);
    divDeposito.append(depositoEnvoltorio);
    depositoCelda.append(divDeposito);
    rowCuenta.append(depositoCelda);

    var retiroCelda = $('<td></td>');
    var divRetiro = $('<div class="row envoltorio"></div>');
    var retiroInput = $('<div class="col-sm-6"><input type="number" step="any" class="form-control cantidad"></div>');
    var retiroEnvoltorio = $('<div class="col"></div>');
    var retiroBoton = $('<button type="button" class="btn btn-danger  ">Retirar</button>');
    retiroBoton.click(Depositar(cuenta, 2));
    retiroEnvoltorio.append(retiroBoton);
    divRetiro.append(retiroInput);
    divRetiro.append(retiroEnvoltorio);
    retiroCelda.append(divRetiro);
    rowCuenta.append(retiroCelda);

    var movimientos = $('<td><button type="button" data-toggle="modal" data-target="#detalleOperaciones" onClick="PintaMovimientos(' + cuenta.id + ')" class="btn btn-primary">Movimientos</button></td>')
    rowCuenta.append(movimientos);
    
    cuentas.append(rowCuenta)
};

function Depositar(cuenta, tipoDeMovimiento) {
    return function () {        
        
        var deposito = new DepositoFactory();
        deposito.Monto = parseFloat($(this).closest('.envoltorio').find('.cantidad').val())
        deposito.Clasificacion = tipoDeMovimiento;
        deposito.IdCuenta = cuenta.id;

        if (!deposito.Monto || deposito.Monto<0)
        {
            Swal.fire('error', 'especifica un monto',  'error');
            return;
        }

        $.ajax({
            type: "POST",
            contentType: 'application/json',
            dataType: 'json',
            url: ubicacion + 'Cuenta/' + cuenta.id + '/Operaciones',
            data: JSON.stringify(deposito),
            timeout: 100000,
            success: function (data) {
                
                ManejadorUniversal(data, function (x)
                {
                    Swal.fire('¡Correcto!', 'operacion guardada', 'success')

                    $('#cuentas .cuenta').each(function ()
                    {
                        if ($(this).text() == x.id)
                        {
                            $(this).closest('tr').find('.saldo').text(x.saldo)
                        }
                    })
                })
            },
            error: function (e) {
                Swal.fire('error', error, 'error');
            },
            done: function (e) {
            }
        });
        
    };
}

var DepositoFactory = function () {
    return { Id: 0, Clasificacion: 0, Monto: 0, IdCuenta: 0 };
};

function ClienteAlta() {
    var detalle = $('#detalle')

    detalle.empty();
    var clienteRegistro = $('<form>' +
        '<div class= "form-group" >                                                                                   ' +
        '<label for="Nombres">Nombre</label>                                                        ' +
        '<input type="text" class="form-control" id="nombres" >               ' +
        '</div>' +        
        '<div class="form-group">                         ' +
        '<label for="Edad"># Identificacion</label>                                                        ' +
        '<input type="number" class="form-control" id="folio">               ' +
        '</div>' +
        '</form>');

    var clienteBoton = $('<button type="button" class="btn btn-primary">Guardar</button>');
    clienteBoton.click(GuardarCliente);
    clienteRegistro.append(clienteBoton);
    detalle.append(clienteRegistro);
}

function GuardarCliente() {
    //var url = urlApi + 'Cliente';
    var customer = {}
    customer.Nombre = $('#nombres').val();
    customer.Folio = parseInt($('#folio').val(), 10)

    if (customer.Nombre.trim() == '' || !customer.Folio)
    {
        Swal.fire('error', 'debes llenar los campos', 'error');
        return;
    }

    $.ajax({
        type: "POST",
        contentType: 'application/json',
        dataType: 'json',
        url: ubicacion + 'Customer',
        data: JSON.stringify(customer),        
        success: function (data)
        {            
            ManejadorUniversal(data, function (x)
            {
                Swal.fire('¡Correcto!', 'tarjetabiente guardado', 'success')
                PintaCustomer(x)
            })
        },
        error: function (e) {
            Swal.fire('error', error, 'error');
        },
        done: function (e) {
        }
    });
}

var PintaCustomer = function (customer) {
    var tableContent = $('#clientes');    
    
        var row = $('<tr>' +
            '<td>' + customer.folio + '</th>' +
            '<td>' + customer.nombre + '</td>' +
            '<td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" onClick="PintaCuentas(' + customer.id + ')">Ver cuentas</button</td>' +
            '</tr>');
        tableContent.append(row);    
}

function PintaMovimientos(cuenta) {      

        $.ajax({
            type: "GET",            
            url: ubicacion + 'Cuenta/' + cuenta + '/Operaciones',
            async: true
        }).done(function (data) {           
            if (data.clasificacion == 1) {

                var operaciones = $('#operaciones')
                operaciones.empty()

                data.detalle.forEach(x =>
                {
                    var clasificacion = x.clasificacion == 2 ?'text-danger':''

                    operaciones.append('<tr><td>' + x.fechaLetra + '</td><td class="' + clasificacion + '">' + x.monto + '</td></tr>')
                })
            }

            ManejadorUniversal(data, function (x)
            {
                var operaciones = $('#operaciones')
                operaciones.empty()

                x.forEach(x => {
                    var clasificacion = x.clasificacion == 2 ? 'text-danger' : ''

                    operaciones.append('<tr><td>' + x.fechaLetra + '</td><td class="' + clasificacion + '">' + x.monto + '</td></tr>')
                })
            })

        }).fail(function (error) {
            console.log(error)
        });    
}

var ManejadorUniversal = function (resp, manejador)
{
    console.log(resp)

    if (resp.clasificacion == clasificacionAnuncio.success) {
        manejador(resp.detalle)
    }
    else
    {
        Swal.fire('error', resp.error, 'error')
    }
}






